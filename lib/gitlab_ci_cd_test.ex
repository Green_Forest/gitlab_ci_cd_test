defmodule GitlabCiCdTest do
  @moduledoc """
  Documentation for `GitlabCiCdTest`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> GitlabCiCdTest.hello()
      :world

  """
  def hello do
    :world
  end
end
